# Build the manager binary
ARG BASE_IMAGE=golang:1.17
ARG REPO_LOCATION

# Load images
FROM ${REPO_LOCATION}${BASE_IMAGE} as builder

WORKDIR /workspace

# SSH key
ARG SSH_KEY
RUN git config --global url."git@github.com:".insteadOf "https://github.com/" && \
  mkdir -p -m 0600 $HOME/.ssh && ssh-keyscan github.com >> $HOME/.ssh/known_hosts

ENV GOPATH="" GOPRIVATE=github.com/acquia

# Copy the Go Modules manifests
COPY go.mod go.sum ./
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN ssh-agent sh -c 'echo $SSH_KEY | base64 -d | ssh-add - ; go mod download'

# Copy the go source
COPY main.go main.go
COPY apis/ apis/
COPY controllers/ controllers/
COPY pkg/ pkg/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o manager main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/manager .
USER 65532:65532

ENTRYPOINT ["/manager"]
