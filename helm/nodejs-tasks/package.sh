#!/usr/bin/env bash
cd "$(dirname "$0")" || exit
# copy in RBAC rules
svcaccts=$(find ../../tasks/templates/rbac -name '*.yaml' -type f)
for sa in $svcaccts; do
  cp "$sa" "./templates/wf-rbac-$(basename $sa)"
done

if [ "$(uname)" == "Darwin" ]; then
  if ! which gsed &> /dev/null; then
    echo "You are using a Mac and don't have gnu sed (gsed) in your path. Task operator will fail to build properly."
    echo "Install GNU sed with:"
    echo "brew install gnu-sed"
    exit 1
  fi
  SED="gsed"
else
  SED="sed"
fi


# Copy in workflow templates and escape {{ argo directives }}, but then un-escape Helm chart {{ .Values... }} directives
tmpls=$(find ../../tasks/templates/tasks -name '*.yaml' -type f)
for t in $tmpls; do
  ${SED} 's/{{/{{`{{/g' "$t" | ${SED} 's/}}/}}`}}/g' \
    | ${SED} -E -e 's/\{\{`\{\{(.*\.Values\..+)\}\}`\}\}/\{\{\1\}\}/' \
    | ${SED} -E -e 's/\{\{`\{\{(.* template \".+)\}\}`\}\}/\{\{\1\}\}/' > "./templates/wf-task-$(basename "$t")"
done

tmpls=$(find ../../tasks/templates/templates -name '*.yaml' -type f)
for t in $tmpls; do
  ${SED} 's/{{/{{`{{/g' "$t" | ${SED} 's/}}/}}`}}/g' \
    | ${SED} -E -e 's/\{\{`\{\{(.*\.Values\..+)\}\}`\}\}/\{\{\1\}\}/' \
    | ${SED} -E -e 's/\{\{`\{\{(.* template \".+)\}\}`\}\}/\{\{\1\}\}/' > "./templates/wf-tmpl-$(basename "$t")"
done
